﻿using Microsoft.Extensions.DependencyInjection;
using SubwayRoutes.Api.DataAccess.Contracts.Repositories;
using SubwayRoutes.Api.DataAccess.Repositories;
using SubwayRoutes.Api.Services;
using SubwayRoutes.Api.Services.Contracts.Services;

namespace SubwayRoutes.Api.Transversal.Register
{
    public static class IoCRegister
    {
        public static IServiceCollection AddRegistration(this IServiceCollection services)
        {

            AddRegisterServices(services);
            AddRegisterRepositories(services);
            //AddRegisterOthers(services);

            return services;

        }

        private static IServiceCollection AddRegisterServices(IServiceCollection services)
        {

            services.AddTransient<IUserService, UserService>();

            return services;
        }

        private static IServiceCollection AddRegisterRepositories(IServiceCollection services)
        {

            services.AddTransient<IUserRepository, UserRepository>();

            return services;
        }

    }
}